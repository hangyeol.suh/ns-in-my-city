// abstract compact object entitites
class CompactObject {
    constructor(name, color, mmin, mmax) {
        if (this.constructor == CompactObject) {
            throw new Error("Abstract classes can't be instantiated.");
        }

        this.name = name;
        this.color = color;
        this.mmin = mmin;
        this.mmax = mmax;
        this.massRange = mmax - mmin;
    }

    // gives the radius for a chosen mass
    radius(mass) {
        throw new Error("Method 'radius' must be implemented.");
    }

    // signals that the compact object is collapsing to a black hole
    isCollapsing(mass) {
        throw new Error("Method 'isCollapsing' must be implemented.");
    }


    massFromSlider(slider) {
        return (
            this.mmin +
            this.massRange * slider.value / CONFIG.SLIDER_POINTS
        );
    }

    massToSliderValue(mass) {
        return Math.floor(
            (mass - this.mmin) *
            CONFIG.SLIDER_POINTS / this.massRange
        );
    }
}

class NeutronStar extends CompactObject {
    constructor() {
        super("Neutron Star", CONFIG.NS_COLOR,
              CONFIG.MIN_MASS, CONFIG.NS_TMP_MAX_MASS);
        // todo load the mass, radius relationship on a grid
    }

    // returns the radius from the M(R) relation given by the EOS
    radius(mass) {
        // todo: interpolate a real M(R) relation,
        //       for now just linearly interpolates [1, mmax] -> [10, 12]
        const a = 2.0 / (1.0 - CONFIG.NS_TMP_MAX_MASS);
        const b = 12.0 - a;
        return (a*mass + b)*1e3;
    }

    // determines whether NS has reached maximum mass
    isCollapsing(mass) {
        // todo: use maximum mass from a real M(R) relation
        return mass >= CONFIG.NS_TMP_MAX_MASS;
    }
}
const neutronStar = new NeutronStar()

class BlackHole extends CompactObject {
    constructor() {
        super("Black Hole", CONFIG.BH_COLOR,
              CONFIG.MIN_MASS, CONFIG.MAX_MASS);
    }

    // return Swarschild radius
    radius(mass) {
        return PHYS.SCHWARZSCHILD * mass;
    }

    // black holes already collapsed
    isCollapsing(mass) {
        return false;
    }
}
const blackHole = new BlackHole();

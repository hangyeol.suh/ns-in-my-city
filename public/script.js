'use strict'

function initialize() {
    // Your access token can be found at: https://cesium.com/ion/tokens.
    // Replace `your_access_token` with your Cesium ion access token.
    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI4M2E4ODAwNy04YmZkLTQ1MDEtODE5NC00ZDgwODdlOTdlMjciLCJpZCI6ODQ2NjMsImlhdCI6MTY0NjQyOTgyNH0.IcCTfp5EethChOaOuAFpDLUWlghr1MANeewEl9asKxs';

    // Initialize the Cesium Viewer in the HTML element with the `cesiumContainer` ID.
    const viewer = new Cesium.Viewer('cesiumContainer', {
        animation : false,
        baseLayerPicker : false,
        homeButton : false,
        infoBox : false,
        scene3DOnly : true,
        sceneModePicker : false,
        shadows: true,
        terrainProvider: Cesium.createWorldTerrain(),
        terrainShadows : Cesium.ShadowMode.DISABLED,
        timeline: false,
    });
    const camera = viewer.camera;
    const geocoder = viewer.geocoder;
    const scene = viewer.scene;
    const globe = scene.globe;
    const globeEllipsoid = globe.ellipsoid;

    const toolbar = document.getElementById("toolbar");

    scene.msaaSamples = 8;

    function offsetLocationForCamera(location) {
        // Convert location from Cartesian3 to Cartographic.
        const startPoint = Cesium.Cartographic.fromCartesian(location);
        // Create a point just south of the original location.
        const southPoint = startPoint.clone();
        southPoint.latitude -= 1;
        // Create a geodesic curve passing south through the location.
        const geodesic = new Cesium.EllipsoidGeodesic(
            startPoint, southPoint,
            globeEllipsoid,
        );
        // Find a point along geodesic at the fixed offset distance.
        const newPoint = geodesic.interpolateUsingSurfaceDistance(CONFIG.CAMERA_OFFSET_INIT);
        newPoint.height = startPoint.height;
        // Return the result in cartesian coordinates.
        return Cesium.Cartographic.toCartesian(newPoint);
    }

    // Add Cesium OSM Buildings, a global 3D buildings layer.
//    const buildingTileset = viewer.scene.primitives.add(Cesium.createOsmBuildings());

    // Initialize the camera location
    const startingCameraLocation = Cesium.Cartesian3.fromDegrees(
        CONFIG.LONGITUDE_INIT, CONFIG.LATITUDE_INIT,
        CONFIG.CAMERA_HEIGHT_INIT,
    );
    const startingCameraOrientation = {pitch : CONFIG.CAMERA_PITCH_INIT};
    camera.setView({
        destination : offsetLocationForCamera(startingCameraLocation),
        orientation : startingCameraOrientation,
    });

    const startingGroundLocation = Cesium.Cartographic.fromDegrees(
        CONFIG.LONGITUDE_INIT, CONFIG.LATITUDE_INIT,
    );
    const compactObject = new COGeometry(startingGroundLocation);
    compactObject.addEntitiesToViewer(viewer);

    // Callback when a new location has been searched.
    geocoder.viewModel.destinationFound = (viewModel, destination) => {
        // Get destination in cartographic coordinates.
        const newPosition = destinationToCartographic(destination);

        // Move the compact object to the new position.
        compactObject.moveTo(newPosition);

        // Move the camera to the destination.
        newPosition.height = CONFIG.CAMERA_HEIGHT_INIT;
        camera.setView({
            destination: offsetLocationForCamera(
                Cesium.Cartographic.toCartesian(newPosition)
            ),
            orientation: startingCameraOrientation,
        });
    };

    // Create compact object type picker and link with object's type.
    const typePicker = new TypePicker();

    typePicker.addChangeListener(() => {
        compactObject.updateType();
    });

    // Create mass slider and link with object's mass.
    const massSlider = new MassSlider();
    const massDisplay = new MassDisplay(compactObject);

    massSlider.addInputListener(() => {
        compactObject.updateMass();
        massDisplay.update();
    });

    compactObject.resetMassSlider = () => {
        massSlider.resetInput(compactObject.sliderValue);
        massDisplay.update();
    };
    compactObject.updateTypePicker = () => {
        typePicker.updateElement(compactObject.compactObject.name);
    }
}

function destinationToCartographic(destination) {
    if (destination instanceof Cesium.Cartesian3) {
        return Cesium.Cartographic.fromCartesian(destination);
    } else if (destination instanceof Cesium.Rectangle) {
        return Cesium.Rectangle.center(destination);
    }
}

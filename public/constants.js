/* Configuration */
const CONFIG = {
    // Compact object config
    MASS_INIT: 1.3,
    NS_COLOR: Cesium.Color.AZURE,
    BH_COLOR: Cesium.Color.BLACK,

    // Mass limits in Msun
    MIN_MASS: 1.0,
    MAX_MASS: 100.0,

    // Distances and locations
    CAMERA_HEIGHT_INIT: 20000,
    CAMERA_OFFSET_INIT: 50000,
    CAMERA_PITCH_INIT: (Math.PI/180.0) * -20.0,
    COMPACT_OBJECT_HEIGHT_OFFSET: 1000,
    LONGITUDE_INIT: -87.88272990147414,
    LATITUDE_INIT: 43.07825671009025,

    // UI
    SLIDER_POINTS: 1000,

    // TODO: remove when real M(R) relationship implemented
    NS_TMP_MAX_MASS: 2.5,
}

/* Physical constants */
const PHYS = {
    C: 299792458.0,
    G: 6.674e-11,
    MSUN: 1.98840987e+30,
}
PHYS.SCHWARZSCHILD = (
    PHYS.MSUN *
    2.0 * PHYS.G * Math.pow(PHYS.C, -2.0)
)

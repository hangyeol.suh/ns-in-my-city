// generalized compact object geometry, can swap between NS and BH
class COGeometry {
    constructor(initGroundLocation) {
        this.compactObject = neutronStar;
        this.groundLocation = initGroundLocation;
        this.mass = CONFIG.MASS_INIT;
        this.radius = this.compactObject.radius(this.mass);
        this.models = this.initModels();
        this.entity = this.models.NS;
    }

    initModels() {
        const NS = createModel("models/neutron_star.glb");
        NS.position = this.position;
        NS.model.scale = this.radius;

        const BH = createModel("models/black_hole.glb");

        NS.show = true;

        return {
            NS: NS,
            BH: BH,
        };
    }

    addEntitiesToViewer(viewer) {
        for (const [key, val] of Object.entries(this.models)) {
            viewer.entities.add(val);
        }
    }

    get sliderValue() {
        return this.compactObject.massToSliderValue(this.mass);
    }

    get position() {
        return Cesium.Cartesian3.fromRadians(
            this.groundLocation.longitude, this.groundLocation.latitude,
            CONFIG.COMPACT_OBJECT_HEIGHT_OFFSET + this.radius/2,
        );
    }

    moveTo(groundLocation) {
        this.groundLocation = groundLocation;
        this.entity.position = this.position;
    }

    updateMass() {
        // update mass value
        const slider = document.getElementById("massSlider");
        this.mass = this.compactObject.massFromSlider(slider);

        // potentially collapse to a black hole
        const isCollapsing = this.compactObject.isCollapsing(this.mass)
        if (isCollapsing) {
            this.compactObject = blackHole;
            this.entity = this.models.BH;
        }

        // update radius
        this.radius = this.compactObject.radius(this.mass);
        this.updateEntityRadius();

        // change shown entity if collapsed to a black hole
        if (isCollapsing) {
            this.models.BH.show = true;
            this.models.NS.show = false;
            this.resetMassSlider();
            this.updateTypePicker();
        }
    }

    updateEntityRadius() {
        this.entity.model.scale = this.radius;
        this.entity.position = this.position;
    }

    updateType() {
        const slider = document.getElementById("massSlider");
        const select = document.getElementById("typePicker");
        const newType = select.value;

        if (newType == "Neutron Star") {
            this.compactObject = neutronStar;
            this.entity = this.models.NS;

            this.mass = this.compactObject.mmin;
            this.radius = this.compactObject.radius(this.mass);
            this.updateEntityRadius();

            this.models.NS.show = true;
            this.models.BH.show = false;
        } else if (newType == "Black Hole") {
            this.compactObject = blackHole;
            this.entity = this.models.BH;

            this.mass = this.compactObject.mmin;
            this.radius = this.compactObject.radius(this.mass);
            this.updateEntityRadius();

            this.models.BH.show = true;
            this.models.NS.show = false;
        } else {
            console.log(`Unknown compact object type: ${newType}`);
        }

        this.resetMassSlider();
    }
}


function createModel(url) {
  return new Cesium.Entity({
      name: url,
      model: {
          uri: url,
          shadows: Cesium.ShadowMode.DISABLED,
      },
      show: false,
  });
}

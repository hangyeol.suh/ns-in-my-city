class MassSlider {
    constructor() {
        this.input = this.createInput();
        this.div = document.getElementById("massSliderContainer");
        this.div.appendChild(this.input);
        this.callbacks = [];
    }

    createInput(initValue = 0) {
        const elem = document.createElement("input");
        elem.setAttribute("type", "range");
        elem.setAttribute("min", 0);
        elem.setAttribute("max", CONFIG.SLIDER_POINTS);
        elem.setAttribute("value", initValue);
        elem.className = "slider";
        elem.id = "massSlider";
        return elem;
    }

    resetInput(value) {
        console.log(`Got value ${value}`);
        // Create new slider.
        const newInput = this.createInput(value);
        console.log("created new");
        // Delete old slider from div.
        this.div.removeChild(this.input);
        console.log("removed old");
        // Add new slider to div.
        this.div.appendChild(newInput);
        console.log("added new");
        // Add new slider to class
        this.input = newInput;
        console.log("saved old");
        // Reapply listeners to new slider
        this.reapplyInputListeners();
        console.log("reapplied listeners");
    }

    addInputListener(callback) {
        this.callbacks.push(callback);
        this.input.addEventListener("input", callback);
    }

    reapplyInputListeners() {
        for (const callback of this.callbacks) {
            this.input.addEventListener("input", callback);
        }
    }
}

class MassDisplay {
    constructor(compactObject) {
        this.compactObject = compactObject;

        this.div = document.getElementById("massDisplayContainer");
        this.div.innerHTML = this.text;
    }

    get slider() {
        return document.getElementById("massSlider");
    }

    get text() {
        const mass = this.compactObject.compactObject.massFromSlider(this.slider);
        return `${mass.toFixed(1)} &times; the Sun's mass`;
    }

    update() {
        this.div.innerHTML = this.text;
    }
}

class TypePicker {
    constructor() {
        this.select = document.getElementById("typePicker");
        this.addOption("Neutron Star");
        this.addOption("Black Hole");
    }

    addOption(name) {
        const index = this.select.options.length;
        const option = document.createElement("option");
        option.text = name;

        this.select.options.add(option, index);
    }


    addChangeListener(callback) {
        this.select.addEventListener("change", callback);
    }

    updateElement(value) {
        this.select.value = value;
    }
}
